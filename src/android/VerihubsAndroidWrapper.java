package com.verihubs.android.plugin.liveness;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.os.Bundle;
import android.content.Context;

//import com.verihubs.msdk_bca.Verihubs;
//import static com.verihubs.msdk_bca.VerihubsType.LIVENESS_CODE;
//import com.verihubs.msdk_bca.VerihubsString;
//
//import static com.verihubs.msdk_bca.VerihubsType.RESULT_ACTIVE_FAIL;
//import static com.verihubs.msdk_bca.VerihubsType.RESULT_PASSIVE_FAIL;
import com.verihubs.bcad.Verihubs;
import com.verihubs.bcad.VerihubsAsset;
import com.verihubs.bcad.VerihubsString;

import static com.verihubs.layout.VerihubsType.LIVENESS_CODE;
import static com.verihubs.layout.VerihubsType.NO_LIVENESS_CODE;

public class VerihubsAndroidWrapper extends CordovaPlugin {

    public boolean isStoragePermissionGranted() {
        Context context = this.cordova.getActivity().getApplicationContext();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context,android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this.cordova.getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    private static int RESULT_OK = -1;
    private static int RESULT_CANCELED = 0;
    private static int MODE_PRIVATE = 0;
    private static CallbackContext callback;
    private Verihubs obj;
    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) {

        this.callback = callbackContext;

        if(action.equals("initClass")){
            obj = new Verihubs(cordova.getActivity());

            isStoragePermissionGranted();

            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.callback.sendPluginResult(pluginResult);
            this.callback.success();
            return true;
        }

        if(action.equals("verifyLiveness")){
            int instructions_count;
            int timeout;
            int[] custom_instructions;
            boolean[] attributes_check;
            JSONArray json_custom_instructions;
            JSONArray json_attributes_check;
//            VerihubsString stringObj = new VerihubsString();
            try{
                instructions_count = args.getInt(0);
                timeout = args.getInt(1);
                json_custom_instructions = args.getJSONArray(2);
                json_attributes_check = args.getJSONArray(3);
                custom_instructions = new int[json_custom_instructions.length()];
                for(int i = 0; i < json_custom_instructions.length(); ++i){
                    custom_instructions[i] = json_custom_instructions.getInt(i);
                }
                attributes_check = new boolean[json_attributes_check.length()];
                for(int i = 0; i < json_attributes_check.length(); ++i){
                    attributes_check[i] = json_attributes_check.getBoolean(i);
                }

                //tutorial-strings
                VerihubsString.getInstance().setFirstTutorialText("Pastikan wajahmu kelihatan jelas. \n\u2022Lepaskan kacamata \n\u2022Lepaskan topi \n\u2022Lepaskan masker");
                VerihubsString.getInstance().setSecondTutorialText("Pegang handphone setinggi mata dan pastikan kamu berada di lokasi dengan sinyal dan penerangan yang baik.");
                VerihubsString.getInstance().setThirdTutorialText("Pastikan wajahmu berada dalam lingkaran.");
                VerihubsString.getInstance().setFirstTutorialButton("Oke");
                VerihubsString.getInstance().setSecondTutorialButton("Saya sudah siap");

                VerihubsAsset.getInstance().setTutorialTextColor("#505050");

                //liveness layout -- color
                VerihubsAsset.getInstance().setLivenessBackgroundColor("#ffffff");
                VerihubsAsset.getInstance().setLivenessInvertedCircleColor("#ffffff");
                VerihubsAsset.getInstance().setLivenessProgressColor("#ffffff");

                //liveness -- text color
                VerihubsAsset.getInstance().setLivenessInstructionsTextColor("#000000");
                VerihubsAsset.getInstance().setLivenessWarningTextColor("#880000");
                VerihubsAsset.getInstance().setLivenessReminderTextColor("#FFFFFF");
                VerihubsAsset.getInstance().setLivenessRulesTextColor("#3193EA");
                VerihubsAsset.getInstance().setLivenessProcessingTextColor("#505050");

                //liveness instructions -- texts
                VerihubsString.getInstance().setHeadLookLeftInstruction("Hadap kiri");
                VerihubsString.getInstance().setHeadLookRightInstruction("Hadap kanan");
                VerihubsString.getInstance().setHeadLookStraightInstruction("Hadap depan");
                VerihubsString.getInstance().setMouthOpenInstruction("Buka dan tutup mulut");

                //liveness warning -- warning
                VerihubsString.getInstance().setRemoveMaskWarning("Lepaskan masker");
                VerihubsString.getInstance().setRemoveSunglassesWarning("Lepaskan kacamata");
                VerihubsString.getInstance().setBlurDetectedWarning("Pastikan smartphone tidak bergoyang");
                VerihubsString.getInstance().setDarkDetectedWarning("Pastikan berada di ruangan yang cukup terang");
                VerihubsString.getInstance().setFaceTooFarWarning("Dekatkan wajah");

                //bottom-sheet -- texts
                VerihubsString.getInstance().setWelcomeText("Halo, ");
                VerihubsString.getInstance().setIntroduceText("Sebelum memulai, pastikan sudah mengikuti langkah - langkah dibawah ini: ");
                VerihubsString.getInstance().setFirstBoxText("Pastikan tidak sedang menggunakan atribut : Masker/Topi/ Kacamata hitam.");
                VerihubsString.getInstance().setSecondBoxText("Pastikan smartphone mempunyai signal yang baik dan cahaya yang cukup terang.");
                VerihubsString.getInstance().setThirdBoxText("Pastikan seluruh area wajah berada dalam lingkaran.");
                VerihubsString.getInstance().setFourthBoxText("Pastikan saat berada di dalam lingkaran smartphone dan wajah tidak bergerak");
                VerihubsString.getInstance().setButtonText("Lanjutkan");

                //liveness bottom -- texts
                VerihubsString.getInstance().setFirstRuleText("Pastikan seluruh area wajah berada dalam lingkaran");
                VerihubsString.getInstance().setSecondRuleText("Pastikan posisi camera setinggi mata");
                VerihubsString.getInstance().setReminderMessage("Selama proses berlangsung seluruh area wajah diharuskan berada dalam lingkaran");
            }
            catch(JSONException e){
                callbackContext.error("Error encountered: " + e.getMessage());
                return false;
            }

            cordova.setActivityResultCallback(this);
            // obj.setTheme(R.style.AppFullScreenTheme);

            //liveness instructions -- asset
            VerihubsAsset.getInstance().setLookLeft(getAppResource("lea_look_left","raw"));
            VerihubsAsset.getInstance().setLookRight(getAppResource("lea_look_right","raw"));
            VerihubsAsset.getInstance().setOpenMouth(getAppResource("lea_open_mouth","raw"));
            VerihubsAsset.getInstance().setLookStraight(getAppResource("verihubs_bcad_lea_look_straight","drawable"));
            VerihubsAsset.getInstance().setBackButton(getAppResource("verihubs_black_back_button","drawable"));
            VerihubsAsset.getInstance().setStartInstructions(getAppResource("steady_phone_resize","drawable"));

            //liveness instructions -- warning assets
            VerihubsAsset.getInstance().setFirstRule(getAppResource("verihubs_face_in_circle_svg","drawable"));
            VerihubsAsset.getInstance().setSecondRule(getAppResource("verihubs_hold_phone_svg","drawable"));

            VerihubsAsset.getInstance().setWarningBlur(getAppResource("steady_phone_resize", "drawable"));
            VerihubsAsset.getInstance().setWarningDark(getAppResource("turn_on_light_resize", "drawable"));
            VerihubsAsset.getInstance().setWarningMask(getAppResource("mask_hat_glass_resize", "drawable"));
            VerihubsAsset.getInstance().setWarningSunglass(getAppResource("mask_hat_glass_resize", "drawable"));
            VerihubsAsset.getInstance().setWarningFaceTooFar(getAppResource("verihubs_bcad_lea_look_straight", "drawable"));
            VerihubsAsset.getInstance().setWarningProcessing(getAppResource("processing_resize", "drawable"));

            //bottom-sheet -- layout
            VerihubsAsset.getInstance().setBottomSheetBackground(getAppResource("verihubs_container_white", "drawable"));
            VerihubsAsset.getInstance().setBottomSheetDropDownBackground(getAppResource("verihubs_container_grey", "drawable"));
            VerihubsAsset.getInstance().setBottomSheetBoxesBackground(getAppResource("verihubs_inst_container", "drawable"));

            //bottom-sheet -- image
            VerihubsAsset.getInstance().setBottomSheetFirstBox(getAppResource("verihubs_all_attributes_svg", "drawable"));
            VerihubsAsset.getInstance().setBottomSheetSecondBox(getAppResource("verihubs_light_connection", "drawable"));
            VerihubsAsset.getInstance().setBottomSheetThirdBox(getAppResource("verihubs_faces_svg", "drawable"));
            VerihubsAsset.getInstance().setBottomSheetFourthBox(getAppResource("verihubs_shake_phone", "drawable"));
            VerihubsAsset.getInstance().setBottomSheetButtonReady(getAppResource("verihubs_button_blue", "drawable"));
            VerihubsAsset.getInstance().setBottomSheetButtonNotReady(getAppResource("verihubs_button_gray", "drawable"));
            VerihubsAsset.getInstance().setBottomSheetBackButton(getAppResource("verihubs_black_back_button", "drawable"));

            //liveness container
            VerihubsAsset.getInstance().setLivenessBottomLayoutWarningColor(getAppResource("verihubs_container_white", "drawable"));
            VerihubsAsset.getInstance().setLivenessBottomLayoutInstructionsColor(getAppResource("verihubs_container_white", "drawable"));
            VerihubsAsset.getInstance().setLivenessBottomLayoutRulesColor(getAppResource("verihubs_container_white", "drawable"));
            VerihubsAsset.getInstance().setLivenessBottomLayoutBackground(getAppResource("verihubs_container_blue_bcad", "drawable"));

            obj.verifyLiveness(true);
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.callback.sendPluginResult(pluginResult);
            return true;
        }

        if(action.equals("getVersion")){
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.callback.sendPluginResult(pluginResult);
            try{
                JSONObject jsonResult = new JSONObject();
                jsonResult.put("version", "1.7.1");
                this.callback.success(jsonResult);
            }catch(JSONException e){
                this.callback.error("Error encountered: " + e.getMessage());
            }
        }

        callbackContext.error("\"" + action + "\" is not a recognized action.");
        return false;
    }

    private int getAppResource(String name, String type) {
        return cordova.getActivity().getResources().getIdentifier(name, type, cordova.getActivity().getPackageName());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data == null){
            this.callback.error("Null error detected");
        }else

        if (requestCode == LIVENESS_CODE)
        {
            if(resultCode == RESULT_OK)
            {
                int status = data.getIntExtra("status", 500);
                int total_instruction = data.getIntExtra("total_instruction", 0);
                SharedPreferences sp = cordova.getActivity().getSharedPreferences("verihubs-storage", MODE_PRIVATE);
                try{
                        JSONObject jsonResult = new JSONObject();
                        jsonResult.put("status", status);
                        jsonResult.put("total_instruction", total_instruction);

                        for(int i=1; i<=total_instruction; ++i){
                                String encoded = sp.getString("image" + (i-1), "");
                                jsonResult.put("base64String_" + i, encoded);
                                String instruction = data.getStringExtra("instruction" + i);
                                jsonResult.put("instruction" + i, instruction);
                                int encoded_length = sp.getInt("length" + (i-1), 0);
                                jsonResult.put("base64StringLength_" + i, encoded.length());
                                if(encoded.length() != encoded_length){
                                    Toast.makeText(cordova.getActivity(), "Length not same", Toast.LENGTH_SHORT).show();
                                }
                        }
                        obj.clean();
                        this.callback.success(jsonResult);
                }
                catch(JSONException e){
                        this.callback.error("Error encountered: " + e.getMessage());
                }
            }
        }
    }
}